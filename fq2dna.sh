#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  fq2dna: paired-end FASTQ-formatted reads to de novo assembly                                              #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2016-2025 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)                         crbip.pasteur.fr  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=25.01;                                                                                             #
# + debugging issue #123 by removing $TMP_DIR/aln.bam before merging (if required)                           #
# + new accuracy index                                                                                       #
# + tested with up-to-date dependancy versions                                                               #
#                                                                                                            #
# VERSION=24.02;                                                                                             #
# + improved check dependencies using option -d                                                              #
# + use the more efficient unimodality (momo) index instead of the bimodality coefficient                    #
#                                                                                                            #
# VERSION=23.12;                                                                                             #
# + new option -d to check dependencies                                                                      #
# + modified default Phred score cutoff, i.e. -q 20                                                          #
# + modified default MINCOV and MAXCOV values, i.e. (3,60) instead of (3,61)                                 #
# + modified estimate of the genome length, using NTCARD with k=25                                           #
# + only 1 thread for step M: slower but reproductible (FLASH output depends on the no. threads)             #
# + now requires SAMTOOLS v1.18                                                                              #
# + improved assembly polishing step for strategies A, B, P, S and V (option -s)                             #
# + only undefined (N) position with enough coverage are written into output file .scf.amb.txt               #
#                                                                                                            #
# VERSION=23.07;                                                                                             #
# + fixed bug with option -w                                                                                 #
# + based on FQCLEANER v23.07                                                                                #
# + MINIMAP2 replaced with BWA-MEM2                                                                          #
# + less conservative genome size approximation, i.e. gsize=F0-f1-f2                                         #
# + modified default MINCOV and MAXCOV values, i.e. (3,61) instead of (2,60)                                 #
# + modified k-mer size selection for SPADES                                                                 #
# + fast assembly polishing step based on SAMTOOLS for strategies A, B, P, S and V (option -s)               #
# + new output file .scf.amb.txt                                                                             #
#                                                                                                            #
# VERSION=21.06ac;                                                                                           #
# + complete updating of the code                                                                            #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- extensions -------------------------------------------------------------------------------------------  #
#                                                                                                            #
  declare -a FILE_EXTENSIONS=("fq" "fastq" "gz" "bz" "bz2" "dsrc" "dsrc2");
#                                                                                                            #
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                  |         #
# A (hidden) slight digital normalization is performed during the first processing step, i.e. DT(C)NE. This  #
# enables observing faster runnning  times during the next  error correction step (E),  especially when the  #
# input sequencing reads induce very large coverage depth, e.g. > 300x.  The static constant MAXMAXCOV sets  #
# the max coverage associated  to this first digital  normalization step.  The value should however be kept  #
# large as the resulting sequencing read set is also used during the assembly polishing step.                #
# Note that the specified MAXCOV (option -C) is always lower than this static constant.                      #
#                                                                                                            #
  MAXMAXCOV=200;
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
  [ "$1" == "-d" ] && NOCHECK=false || NOCHECK=true;
#                                                                                                            #
# - SUMMARY -                                                                                                #
# AlienDiscover/0.1 AlienRemover/1.0 AlienTrimmer/2.1 aragorn/1.2.41 bedtools/2.29.2 blast+/2.16.0 bwa_mem2/2.2.1 bzip2/1.0.6 contig_info/v2.01 diamond/2.0.6 dsrc/2.0.2 FASTA2AGP/2.0 fqtools/1.2 FLASH/1.2.11 gawk/5.0.1 gnuplot/5.2.8 hmmer/3.4 infernal/1.1.5 graalvm/ce-java11-20.0.0 minced/0.4.2 MUMmer/4.0.0rc1 musket/1.1 ncbitools/20170106 ntCard/1.2.2 prodigal/2.6.3.pp1 ROCK/2.0.0 samtools/1.21 SAM2MSA/0.4.3.1 signalp/6.0 barrnap/0.9 fqCleanER/23.12 platon/1.6 prokka/1.14.5 SPAdes/4.0.0 
#                                                                                                            #
# -- gawk version >= 4 ------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  if $NOCHECK && [ ! $(command -v $GAWK_BIN) ]; then echo "$GAWK_BIN not found"               >&2 ; exit 1 ; fi
  BAWK="$GAWK_BIN";
  CAWK="$GAWK_BIN -F,";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- bwa-mem2: read alignment -----------------------------------------------------------------------------  #
#                                                                                                            #
  BWAMEM2_BIN=bwa-mem2;
  if $NOCHECK && [ ! $(command -v $BWAMEM2_BIN) ]; then echo "$BWAMEM2_BIN not found"         >&2 ; exit 1 ; fi
  BWA_INDEX="$BWAMEM2_BIN index";
  BWA_MEM="$BWAMEM2_BIN mem -v 0 -K 100000000 -Y -L 20 -D 0.1 -r 0.1"; # [-D 0.5] Reducing -D increases accuracy but decreases the mapping speed (from github.com/lh3/bwa/blob/master/NEWS.md)
                                                                       # [-r 1.5] Larger value yields fewer seeds, which leads to faster alignment speed but lower accuracy (from bio-bwa.sourceforge.net/bwa.shtml)
                                                                       # see also github.com/nf-core/sarek/issues/101
                                                                       #          doi.org/10.1093/bioinformatics/btw040
#                                                                                                            #
# -- contig_info: sequence file descriptive statistics ----------------------------------------------------  #
#                                                                                                            #
  CONTIG_INFO_BIN=contig_info;
  if $NOCHECK && [ ! $(command -v $CONTIG_INFO_BIN) ]; then echo "$CONTIG_INFO_BIN not found" >&2 ; exit 1 ; fi
  CONTIG_INFO_STATIC_OPTIONS="";
  CONTIG_INFO="$CONTIG_INFO_BIN $CONTIG_INFO_STATIC_OPTIONS";
#                                                                                                            #
# -- FASTA2AGP: unscaffolding -----------------------------------------------------------------------------  #
#                                                                                                            #
  FASTA2AGP_BIN=FASTA2AGP;
  if $NOCHECK && [ ! $(command -v $FASTA2AGP_BIN) ]; then echo "$FASTA2AGP_BIN not found"     >&2 ; exit 1 ; fi
  FASTA2AGP_STATIC_OPTIONS="-n 8";
  FASTA2AGP="$FASTA2AGP_BIN $FASTA2AGP_STATIC_OPTIONS";
#                                                                                                            #
# -- fqCleanER: FASTQ file preprocessing ------------------------------------------------------------------  #
#                                                                                                            #
  FQCLEANER_BIN=fqCleanER;
  if $NOCHECK && [ ! $(command -v $FQCLEANER_BIN) ]; then echo "$FQCLEANER_BIN not found"     >&2 ; exit 1 ; fi
  FQCLEANER_STATIC_OPTIONS="";
  FQCLEANER="$FQCLEANER_BIN $FQCLEANER_STATIC_OPTIONS";
#                                                                                                            #
# -- fqtools: FASTQ file utilities (fqstats) --------------------------------------------------------------  #
#                                                                                                            #
  FQSTATS_BIN=fqstats;
  if $NOCHECK && [ ! $(command -v $FQSTATS_BIN) ]; then echo "$FQSTATS_BIN not found"         >&2 ; exit 1 ; fi
  FQSTATS_STATIC_OPTIONS="";
  FQSTATS="$FQSTATS_BIN $FQSTATS_STATIC_OPTIONS";
#                                                                                                            #
# -- ntCard: estimating occurrences of distinct canonical k-mers ------------------------------------------  # 
#                                                                                                            #
  NTCARD_BIN=ntcard;
  if $NOCHECK && [ ! $(command -v $NTCARD_BIN) ]; then echo "$NTCARD_BIN not found"           >&2 ; exit 1 ; fi
  NTCARD_STATIC_OPTIONS="-k 25";
  NTCARD="$NTCARD_BIN $NTCARD_STATIC_OPTIONS";
#                                                                                                            #
# -- PLATON: chromosome/plasmid classification ------------------------------------------------------------  #
#                                                                                                            #
  PLATON_BIN=platon;
  if $NOCHECK && [ ! $(command -v $PLATON_BIN) ]; then echo "$PLATON_BIN not found"           >&2 ; exit 1 ; fi
  PLATON_STATIC_OPTIONS="--mode sensitivity --characterize ";
  PLATON="$PLATON_BIN $PLATON_STATIC_OPTIONS";
  PLATON_SENS=-7.9; # any seq. with RDS lower than this cutoff likely arises from chromosome (FPP<=5%)
  PLATON_SPEC=0.7;  # any seq. with RDS higher than this cutoff likely arises from plasmid (FPP<=0.1%)
#                                                                                                            #
# -- PROKKA: genome annotation ----------------------------------------------------------------------------  #
#                                                                                                            #
  PROKKA_BIN=prokka;
  if $NOCHECK && [ ! $(command -v $PROKKA_BIN) ]; then echo "$PROKKA_BIN not found"           >&2 ; exit 1 ; fi
  PROKKA_STATIC_OPTIONS="--quiet --addgenes ";
  PROKKA="$PROKKA_BIN $PROKKA_STATIC_OPTIONS";
#                                                                                                            #
# -- samtools: exploring read alignments ------------------------------------------------------------------  #
#                                                                                                            #
  SAMTOOLS_BIN=samtools;
  if $NOCHECK && [ ! $(command -v $SAMTOOLS_BIN) ]; then echo "$SAMTOOLS_BIN not found"       >&2 ; exit 1 ; fi
  SAMTOOLS_SORT="$SAMTOOLS_BIN sort --output-fmt BAM -l 1 -m 1073741824";
  SAMTOOLS_MERGE="$SAMTOOLS_BIN merge --output-fmt BAM"; 
  SAMTOOLS_CONSENSUS="$SAMTOOLS_BIN consensus --format FASTA --show-del no --show-ins yes"; 
#                                                                                                            #
# -- SAM2MAP: genome coverage profile assessment ----------------------------------------------------------  #
#                                                                                                            #
  SAM2MAP_BIN=SAM2MAP;
  if $NOCHECK && [ ! $(command -v $SAM2MAP_BIN) ]; then echo "$SAM2MAP_BIN not found"         >&2 ; exit 1 ; fi
  SAM2MAP_STATIC_OPTIONS="-Q 20 -f 0.75 -p 0.000001";
  SAM2MAP="$SAM2MAP_BIN $SAM2MAP_STATIC_OPTIONS";
#                                                                                                            #
# -- SPAdes: de novo assembly -----------------------------------------------------------------------------  #
#                                                                                                            #
  SPADES_BIN=spades.py;
  if $NOCHECK && [ ! $(command -v $SPADES_BIN) ]; then echo "$SPADES_BIN not found"           >&2 ; exit 1 ; fi
  SPADES_STATIC_OPTIONS="--phred-offset 33 --cov-cutoff auto ";
  SPADES="$SPADES_BIN $SPADES_STATIC_OPTIONS";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ===========================                                                                                #
# = CHECK DEPENDENCIES      =                                                                                #
# ===========================                                                                                #
#                                                                                                            #
  if ! $NOCHECK
  then
    echo "Checking required dependencies ..." ;
    ## bc ##############################
    echo -e -n "> \e[1mbc\e[0m          >0         \t" ;                binexe=bc;
    echo -e -n "$binexe      \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   bc -v | head -1 ;
    fi
    ## grep ############################
    echo -e -n "> \e[1mgrep\e[0m        >0         \t" ;                binexe=grep;
    echo -e -n "$binexe    \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   grep -V | head -1 ;
    fi
    ## sed #############################
    echo -e -n "> \e[1msed\e[0m         >0         \t" ;                binexe=sed;
    echo -e -n "$binexe     \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   sed --version | head -1 ;
    fi
    ## gawk >=4.0.0 ####################
    echo -e -n "> \e[1mgawk\e[0m        >4.0.0     \t" ;                binexe=$GAWK_BIN;
    echo -e -n "$binexe    \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $GAWK_BIN -V | head -1 ;
    fi
    ## bwa-mem2 >=2.2.1 ################
    echo -e -n "> \e[1mbwa-mem2\e[0m    >=2.2.1    \t" ;                binexe=$BWAMEM2_BIN;
    echo -e -n "$binexe\t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $BWAMEM2_BIN version ;
    fi
    ## contig_info >2.0 ################
    echo -e -n "> \e[1mcontig_info\e[0m >2.0       \t" ;                binexe=$CONTIG_INFO_BIN;
    echo -e -n "$binexe\t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $CONTIG_INFO_BIN -h | sed -n 2p | sed 's/Copyright.*//g' ;
    fi
    ## FASTA2AGP >=2.0 #################
    echo -e -n "> \e[1mFASTA2AGP\e[0m   >=2.0      \t" ;                binexe=$FASTA2AGP_BIN;
    echo -e -n "$binexe\t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $FASTA2AGP_BIN | sed -n 2p | sed 's/Copyright.*//g' ;
    fi
    ## fqCleanER >=23.12 ###############
    echo -e -n "> \e[1mfqCleanER\e[0m   >=23.12    \t" ;                binexe=$FQCLEANER_BIN;
    echo -e -n "$binexe\t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $FQCLEANER_BIN -h | sed -n 2p | sed 's/Copyright.*//g' ;
    fi
    ## fqstats >=1.2 ###################
    echo -e -n "> \e[1mfqstats\e[0m     >=1.2      \t" ;                binexe=$FQSTATS_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   echo ;
    fi
    ## ntCard >1.2 #####################
    echo -e -n "> \e[1mntCard\e[0m      >1.2      \t" ;                 binexe=$NTCARD_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $NTCARD_BIN --version 2>&1 | head -1 ;
    fi
    ## PLATON >1.5 #####################
    echo -e -n "> \e[1mPlaton\e[0m      >1.5       \t" ;                binexe=$PLATON_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $PLATON_BIN --version ;
    fi
    ## PROKKA >=1.14.5 #################
    echo -e -n "> \e[1mProkka\e[0m      >=1.14.5   \t" ;                binexe=$PROKKA_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $PROKKA_BIN --version ;
    fi
    ## samtools >= 1.18 ################
    echo -e -n "> \e[1msamtools\e[0m    >=1.18     \t" ;                binexe=$SAMTOOLS_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $SAMTOOLS_BIN --version | head -1;
    fi
    ## SAM2MAP >=0.4 ###################
    echo -e -n "> \e[1mSAM2MAP\e[0m     >=0.4      \t" ;                binexe=$SAM2MAP_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $SAM2MAP_BIN | sed -n 2p | sed 's/Copyright.*//g' ;
    fi
    ## SPAdes >=3.15.5 #################
    echo -e -n "> \e[1mSPAdes\e[0m      >=3.15.5   \t" ;                binexe=$SPADES_BIN;
    echo -e -n "$binexe  \t\t" ;
    if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
    else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $SPADES_BIN --version ;
    fi
    echo "[exit]" ;
    exit ;
  fi
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m fq2dna v$VERSION                                           $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/fq2dna
 https://research.pasteur.fr/en/tool/fq2dna

 USAGE:  fq2dna.sh  [options] 

 Processing and assembling high-throughput sequencing (HTS) paired-end (PE) reads:
  + processing HTS reads  using different steps:  deduplicating [D], trimming/clipping [T], error
    correction [E], contaminant removal [C], merging [M], and/or digital normalization [N] 
  + de novo assembly [dna] of  the whole genome from  processed HTS reads,  followed by polishing
    and annotation steps (depending on the specified strategy) 

 OPTIONS:
  -1 <infile>   fwd (R1) FASTQ input file name from PE library 1 || input files can be compressed
  -2 <infile>   rev (R2) FASTQ input file name from PE library 1 || using   either   gzip   (file
  -3 <infile>   fwd (R1) FASTQ input file name from PE library 2 || extension .gz), bzip2 (.bz or
  -4 <infile>   rev (R2) FASTQ input file name from PE library 2 || .bz2),   or  DSRC  (.dsrc  or
  -5 <infile>   fwd (R1) FASTQ input file name from PE library 3 || .dsrc2), or uncompressed (.fq 
  -6 <infile>   rev (R2) FASTQ input file name from PE library 3 || or .fastq)                 
  -o <outdir>   path and name of the output directory (mandatory option)
  -b <string>   base name for output files (mandatory option)
  -s <char>     to set a predefined strategy for processing HTS reads among the following ones:
                  A   Archaea:     DT(C)E + [N|MN] + dna + polishing + annotation
                  B   Bacteria:    DT(C)E + [N|MN] + dna + polishing + annotation 
                  E   Eukaryote:   DT(C)  + [N|MN] + dna
                  P   Prokaryote:  DT(C)E + [N|MN] + dna + polishing
                  S   Standard:    DT(C)  + [N|MN] + dna + polishing
                  V   Virus:       DT(C)E + [N|MN] + dna + polishing + annotation
                (default: S)
  -L <int>      minimum required length for a contig (default: 300)
  -T <"G S I">  Genus (G), Species (S) and Isolate (I)  names to be used  during annotation step;
                should be set between quotation  marks and separated by a blank space;  only with
                options -s A, -s B or -s V (default: "Genus sp. STRAIN")
  -q <int>      quality score threshold;  all bases  with Phred  score below  this threshold  are 
                considered as non-confident during step [T] (default: 20)
  -l <int>      minimum required length for a read (default: half the average read length)
  -p <int>      maximum allowed percentage  of non-confident bases  (as ruled  by option -q)  per 
                read (default: 50) 
  -c <int>      minimum allowed coverage depth during step [N] (default: 3)
  -C <int>      maximum allowed coverage depth during step [N] (default: 60)
  -a <infile>   to set a file containing every  alien oligonucleotide sequence  (one per line) to
                be clipped during step [T] (see below)
  -a <string>   one or several key words (separated with commas),  each corresponding to a set of
                alien oligonucleotide sequences to be clipped during step [T]:
                  POLY                nucleotide homopolymers
                  NEXTERA             Illumina Nextera index Kits
                  IUDI                Illumina Unique Dual index Kits
                  AMPLISEQ            AmpliSeq for Illumina Panels
                  TRUSIGHT_PANCANCER  Illumina TruSight RNA Pan-Cancer Kits
                  TRUSEQ_UD           Illumina TruSeq Unique Dual index Kits
                  TRUSEQ_CD           Illumina TruSeq Combinatorial Dual index Kits
                  TRUSEQ_SINGLE       Illumina TruSeq Single index Kits
                  TRUSEQ_SMALLRNA     Illumina TruSeq Small RNA Kits
                Note that these sets of alien sequences are not exhaustive and will never replace
                the exact oligos used for library preparation (default: "POLY")
  -a AUTO       to (try to)  infer 3' alien  oligonucleotide  sequence(s) for step [T];  inferred
                oligo(s) are completed with those from "POLYS" (see above)                
  -A <infile>   to set  sequence or  k-mer model  file(s) to carry  out contaminant  read removal 
                during step [C];  several comma-separated  file names  can be specified;  allowed 
                file extensions: .fa, .fasta, .fna, .kmr or .kmz
  -t <int>      number of threads (default: 12)
  -w <dir>      path to tmp directory (default: \$TMPDIR, otherwise /tmp)
  -x            to not remove (after completing) the tmp directory inside the one set with option
                -w (default: not set)
  -d            checks dependencies and exit
  -h            prints this help and exit

  EXAMPLES:
    fq2dna.sh  -1 r1.fastq -2 r2.fastq  -o out -b ek12  -s P -T "Escherichia coli K12" -a AUTO 
    fq2dna.sh  -1 rA.1.fq -2 rA.2.fq -3 rB.1.fq.gz -4 rB.2.fq.gz  -o out -b name  -a NEXTERA
 
EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- runcmd -----------------------------------------------------------------------------------------------  #
# >> executes the specified command line $1; exit 1 if the command line returns an error signal              #
#                                                                                                            #
runcmd () {
   sh -c "$*" || { echo "FAIL: $*" ; exit 1 ; } ;
}
#                                                                                                            #
# -- fb ---------------------------------------------------------------------------------------------------  #
# >> returns the specified byte size $1 in rounded format                                                    #
#                                                                                                            #
fb() {
  if   [ $1 -gt 1073741824 ]; then echo "$(bc -l <<<"scale=1;$1/1073741824" | sed 's/^\./0\./') Gb" ;
  elif [ $1 -gt 1048576 ];    then echo "$(bc -l <<<"scale=1;$1/1048576"    | sed 's/^\./0\./') Mb" ;
  elif [ $1 -gt 1024 ];       then echo "$(bc -l <<<"scale=1;$1/1024"       | sed 's/^\./0\./') kb" ;
  else                             echo "$1 b" ; fi
}
#                                                                                                            #
# -- disp -------------------------------------------------------------------------------------------------  #
# >> displays info for the specified file $1                                                                 #
#                                                                                                            #
disp() {
  echo -e "[$(fb $(stat -c %s $1))]\t$1" ;
}  
#                                                                                                            #
# -- chrono -----------------------------------------------------------------------------------------------  #
# >> returns the elapsed time in well-formatted format                                                       #
#                                                                                                            #
chrono() {
  local s=$SECONDS; printf "[%02d:%02d]" $(( $s / 60 )) $(( $s % 60 )) ;
}
#                                                                                                            #
# -- chext () ---------------------------------------------------------------------------------------------  #
# >> returns "valid" when the specified file $1 extension is valid; "invalid" otherwise                      #
#                                                                                                            #
chext() {
  local f=$1; local fe="${f##*.}"; local out="invalid";
  for e in "${FILE_EXTENSIONS[@]}"
  do
    if [ "$e" == "$fe" ]; then out="valid"; break ; fi
  done
  echo "$out" ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

LIB1=false;
LIB2=false;
LIB3=false;
FQ11="$NA"; FQ12="$NA";   # lib1 PE files                      -1 -2
FQ21="$NA"; FQ22="$NA";   # lib2 PE files                      -3 -4
FQ31="$NA"; FQ32="$NA";   # lib3 PE files                      -5 -6
OUTDIR="$NA";             # outdir                                -o
BASENAME="$NA";           # basename                              -b
TMP_DIR=${TMPDIR:-/tmp};  # tmp directory                         -w
ALIENS="$NA";             # alien oligo                           -a
CONTA="$NA";              # contaminant file                      -A
Q=20;                     # Phred score threshold                 -q
P=50;                     # max allowed percent low-Phreded bases -p
L=0;                      # min allowed lgt                       -l
MINCOV=3;                 # min allowed read coverage depth       -c
MAXCOV=60;                # max allowed read coverage depth       -C
STRATEGY="S";             # read processing strategy              -s [ABEPSV]
MINCTGLGT=300;            # min contig/scaffold lgt               -L
NTHREADS=12;              # no. threads                           -t
RMTMP=true;               # rm tmp dir                            -x
TAXON="Genus sp. STRAIN"; # taxo                                  -T

while getopts :1:2:3:4:5:6:o:b:a:A:q:p:l:c:C:L:s:T:t:w:xh option
do
  case $option in
   1) FQ11="$OPTARG"     ;;
   2) FQ12="$OPTARG"     ;;
   3) FQ21="$OPTARG"     ;;
   4) FQ22="$OPTARG"     ;;
   5) FQ31="$OPTARG"     ;;
   6) FQ32="$OPTARG"     ;;
   o) OUTDIR="$OPTARG"   ;;
   b) BASENAME="$OPTARG" ;;
   a) ALIENS="$OPTARG"   ;;
   A) CONTA="$OPTARG"    ;;
   q) Q=$OPTARG          ;;
   p) P=$OPTARG          ;;
   l) L=$OPTARG          ;;
   c) MINCOV=$OPTARG     ;;
   C) MAXCOV=$OPTARG     ;;
   L) MINCTGLGT=$OPTARG  ;;
   s) STRATEGY="$OPTARG" ;;
   t) NTHREADS=$OPTARG   ;;
   T) TAXON="$OPTARG"    ;;
   w) TMP_DIR="$OPTARG"  ;;
   x) RMTMP=false        ;;
   h)  mandoc ;  exit 0  ;;
   \?) mandoc ;  exit 1  ;;
  esac
done

echo "# fq2dna v$VERSION" ;
echo "# $COPYRIGHT" ;
echo "+ https://gitlab.pasteur.fr/GIPhy/fq2dna" ;
echo "> System: $MACHTYPE" ;
echo "> Bash:   $BASH_VERSION" ;

##############################################################################################################
#                                                                                                            #
# CHECKING OPTIONS                                                                                           #
#                                                                                                            #
##############################################################################################################
## PE lib 1  #################################################################################################
echo "# input file(s):"
NLIB=0;
if [ "$FQ11" != "$NA" ]
then
  if [ ! -e $FQ11 ];                  then echo "lib1 R1 file does not exist (option -1): $FQ11" >&2 ; exit 1 ; fi
  if [ ! -f $FQ11 ];                  then echo "lib1 R1 file is not regular (option -1): $FQ11" >&2 ; exit 1 ; fi
  if [ ! -r $FQ11 ];                  then echo "no read permission (option -1): $FQ11"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ11)" != "valid" ]; then echo "invalid extension (option -1): $FQ11"           >&2 ; exit 1 ; fi
  if [ "$FQ12" == "$NA" ];            then echo "no lib1 R2 file (option -2)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ12 ];                  then echo "lib1 R2 file does not exist (option -2): $FQ12" >&2 ; exit 1 ; fi
  if [ ! -f $FQ12 ];                  then echo "lib1 R2 file is not regular (option -2): $FQ12" >&2 ; exit 1 ; fi
  if [ ! -r $FQ12 ];                  then echo "no read permission (option -2): $FQ12"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ12)" != "valid" ]; then echo "invalid extension (option -2): $FQ12"           >&2 ; exit 1 ; fi
  LIB1=true;
  let NLIB++ ;
  echo "> PE lib 1" ;
  echo -e "+ FQ11: $(disp $FQ11)";
  echo -e "+ FQ12: $(disp $FQ12)"; 
fi
## PE lib 2  #################################################################################################
if [ "$FQ21" != "$NA" ]
then
  if [ ! -e $FQ21 ];                  then echo "lib2 R1 file does not exist (option -3): $FQ21" >&2 ; exit 1 ; fi
  if [ ! -f $FQ21 ];                  then echo "lib2 R1 file is not regular (option -3): $FQ21" >&2 ; exit 1 ; fi
  if [ ! -r $FQ21 ];                  then echo "no read permission (option -3): $FQ21"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ21)" != "valid" ]; then echo "invalid extension (option -3): $FQ21"           >&2 ; exit 1 ; fi
  if [ "$FQ22" == "$NA" ];            then echo "no lib2 R2 file (option -4)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ22 ];                  then echo "lib2 R2 file does not exist (option -4): $FQ22" >&2 ; exit 1 ; fi
  if [ ! -f $FQ22 ];                  then echo "lib2 R2 file is not regular (option -4): $FQ22" >&2 ; exit 1 ; fi
  if [ ! -r $FQ22 ];                  then echo "no read permission (option -4): $FQ22"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ22)" != "valid" ]; then echo "invalid extension (option -4): $FQ22"           >&2 ; exit 1 ; fi
  LIB2=true;
  let NLIB++ ;
  echo "> PE lib 2" ;
  echo -e "+ FQ21: $(disp $FQ21)";
  echo -e "+ FQ22: $(disp $FQ22)"; 
fi
## PE lib 3  #################################################################################################
if [ "$FQ31" != "$NA" ]
then
  if [ ! -e $FQ31 ];                  then echo "lib3 R1 file does not exist (option -5): $FQ31" >&2 ; exit 1 ; fi
  if [ ! -f $FQ31 ];                  then echo "lib3 R1 file is not regular (option -5): $FQ31" >&2 ; exit 1 ; fi
  if [ ! -r $FQ31 ];                  then echo "no read permission (option -5): $FQ31"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ31)" != "valid" ]; then echo "invalid extension (option -5): $FQ31"           >&2 ; exit 1 ; fi
  if [ "$FQ32" == "$NA" ];            then echo "no lib3 R2 file (option -6)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ32 ];                  then echo "lib3 R2 file does not exist (option -6): $FQ32" >&2 ; exit 1 ; fi
  if [ ! -f $FQ32 ];                  then echo "lib3 R2 file is not regular (option -6): $FQ32" >&2 ; exit 1 ; fi
  if [ ! -r $FQ32 ];                  then echo "no read permission (option -6): $FQ32"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ32)" != "valid" ]; then echo "invalid extension (option -6): $FQ32"           >&2 ; exit 1 ; fi
  LIB3=true;
  let NLIB++ ;
  echo "> PE lib 3" ;
  echo -e "+ FQ31: $(disp $FQ31)";
  echo -e "+ FQ32: $(disp $FQ32)"; 
fi
if [ $NLIB -eq 0 ];                             then echo "no input file(s)"                                    >&2 ; exit 1 ; fi
## Q  ########################################################################################################
if ! [[ $Q =~ ^[0-9]+$ ]];                      then echo "incorrect value (option -q): $Q"                     >&2 ; exit 1 ; fi
## L  ########################################################################################################
if ! [[ $L =~ ^[0-9]+$ ]];                      then echo "incorrect value (option -l): $L"                     >&2 ; exit 1 ; fi
## P  ########################################################################################################
if ! [[ $P =~ ^[0-9]+$ ]];                      then echo "incorrect value (option -p): $P"                     >&2 ; exit 1 ; fi
if [ $P -gt 100 ];                              then echo "incorrect percentage (option -p): $P"                >&2 ; exit 1 ; fi
## MINCOV/MAXCOV  ############################################################################################
if ! [[ $MINCOV =~ ^[0-9]+$ ]];                 then echo "incorrect value (option -c): $MINCOV"                >&2 ; exit 1 ; fi
if ! [[ $MAXCOV =~ ^[0-9]+$ ]];                 then echo "incorrect value (option -C): $MAXCOV"                >&2 ; exit 1 ; fi
if [ $MINCOV -ge $MAXCOV ];                     then echo "min too large (options -c/-C): $MINCOV >= $MAXCOV"   >&2 ; exit 1 ; fi
if [ $MAXCOV -gt $MAXMAXCOV ];                  then echo "expected max cov <= $MAXMAXCOV (option -C): $MAXCOV" >&2 ; exit 1 ; fi
## MINCTGLGT  ################################################################################################
if ! [[ $MINCTGLGT =~ ^[0-9]+$ ]];              then echo "incorrect value (option -L): $MINCTGLGT"             >&2 ; exit 1 ; fi
## NTHREADS  #################################################################################################
if ! [[ $NTHREADS =~ ^[0-9]+$ ]];               then echo "incorrect value: $NTHREADS (option -t)"              >&2 ; exit 1 ; fi
[ $NTHREADS -lt 2 ] && NTHREADS=2;
## TMPDIR  ###################################################################################################
[ "${TMP_DIR:0:1}" != "/" ] && TMP_DIR="$(pwd)/$TMP_DIR";
if [ ! -e $TMP_DIR ];                           then echo "tmp directory does not exist (option -w): $TMP_DIR"  >&2 ; exit 1 ; fi
if [ ! -d $TMP_DIR ];                           then echo "not a directory (option -w): $TMP_DIR"               >&2 ; exit 1 ; fi
if [ ! -w $TMP_DIR ];                           then echo "no write permission (option -w): $TMP_DIR"           >&2 ; exit 1 ; fi
## BASENAME  #################################################################################################
if [ "$BASENAME" == "$NA" ];                    then echo "basename not specified (option -b)"                  >&2 ; exit 1 ; fi
## OUTDIR  ###################################################################################################
if [ "$OUTDIR" == "$NA" ];                      then echo "outdir not specified (option -o)"                    >&2 ; exit 1 ; fi
if [ -e $OUTDIR ]
then
  if [ ! -d $OUTDIR ];                          then echo "not a directory (option -o): $OUTDIR"                >&2 ; exit 1 ; fi
  if [ ! -w $OUTDIR ];                          then echo "no write permission (option -o): $OUTDIR"            >&2 ; exit 1 ; fi
fi
## STRATEGY  #################################################################################################
if [[ ! "ABEPSV" =~ $STRATEGY ]];               then echo "incorrect strategy (option -s)"                      >&2 ; exit 1 ; fi
## TAXON  ####################################################################################################
if [ $($BAWK '{print NF}' <<<"$TAXON") -lt 3 ]; then echo "incorrect taxon name (option -T): $TAXO"             >&2 ; exit 1 ; fi

##############################################################################################################
#                                                                                                            #
# INIT                                                                                                       #
#                                                                                                            #
##############################################################################################################

## OUTDIR  ###################################################################################################
if [ "$OUTDIR" == "$NA" ];        then echo "outdir not specified (option -o)"                   >&2 ; exit 1 ; fi
if [ ! -e $OUTDIR ]
then
  mkdir $OUTDIR ;
  if [ ! -e $OUTDIR ];            then echo "unable to create directory (option -o): $OUTDIR"    >&2 ; exit 1 ; fi
fi
echo "# output directory" ; echo "+ OUTDIR=$OUTDIR" ;

## TMPDIR  ###################################################################################################
TMP_DIR=$(mktemp -d -p $TMP_DIR -t fq2dna.$BASENAME.XXXXXXXXX);
echo "# tmp directory" ; echo "+ TMP_DIR=$TMP_DIR" ;

## SYSTEM SPEC  ##############################################################################################
export LC_ALL=C ;
arf() {
  echo -n " [stop] >>" >&2 ;
  sleep 1 ;
  echo -n " [kill jobs] >>" >&2 ;
  kill -9 $(jobs -pr) &> /dev/null ;
  sleep 1 ;
  echo -n ">>" >&2 ;
  wait ;
  echo -n " [rm tmp dir] >" >&2 ;
  while [ -e $TMP_DIR ]
  do
    rm -rf $TMP_DIR &>/dev/null ;
    echo -n ">" >&2 ;
    sleep 1 ;
  done
  echo " [exit]" >&2 ;
}
trap 'arf;exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ; # &> /dev/null ;



##############################################################################################################
#                                                                                                            #
# GO!!                                                                                                       #
#                                                                                                            #
##############################################################################################################

##############################################################################
## processing reads                                                         ##
##############################################################################

### step 1: DT(C)NE ##########################################################
case $STRATEGY in
A) echo "# STRATEGY A: Archaea" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTNE";  MESSAGE="Deduplicating, Trimming, Clipping and Correcting reads";                  
   else steps="DTCNE"; MESSAGE="Deduplicating, Trimming, Clipping, Decontaminating and Correcting reads"; fi ;;
B) echo "# STRATEGY B: Bacteria" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTNE";  MESSAGE="Deduplicating, Trimming, Clipping and Correcting reads";                  
   else steps="DTCNE"; MESSAGE="Deduplicating, Trimming, Clipping, Decontaminating and Correcting reads"; fi ;;
E) echo "# STRATEGY E: Eukayote" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTN";  MESSAGE="Deduplicating, Trimming, and Clipping reads";                 
   else steps="DTCN"; MESSAGE="Deduplicating, Trimming, Clipping and Decontaminating reads";              fi ;;
P) echo "# STRATEGY P: Prokaryote (Bacteria/Archaea)" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTNE";  MESSAGE="Deduplicating, Trimming, Clipping and Correcting reads";                  
   else steps="DTCNE"; MESSAGE="Deduplicating, Trimming, Clipping, Decontaminating and Correcting reads"; fi ;;
S) echo "# STRATEGY S: Standard" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTN";  MESSAGE="Deduplicating, Trimming, and Clipping reads";
   else steps="DTCN"; MESSAGE="Deduplicating, Trimming, Clipping, and Decontaminating reads";             fi ;;
V) echo "# STRATEGY V: Virus" ;
   if [ "$CONTA" == "$NA" ]
   then steps="DTNE";  MESSAGE="Deduplicating, Trimming, Clipping and Correcting reads"
   else steps="DTCNE"; MESSAGE="Deduplicating, Trimming, Clipping, Decontaminating and Correcting reads"; fi ;;
esac
echo -n "$(chrono) $MESSAGE ... " ;

infiles="";
$LIB1 && infiles="-1 $FQ11 -2 $FQ12";
$LIB2 && infiles="$infiles -3 $FQ21 -4 $FQ22";
$LIB3 && infiles="$infiles -5 $FQ31 -6 $FQ32";
options="-o $TMP_DIR -b stepI -q $Q -p $P -c 2 -C $MAXMAXCOV -t $NTHREADS -w $TMP_DIR";
[ $L -gt 0 ]           && options="$options -l $L";
[ "$ALIENS" != "$NA" ] && options="$options -a $ALIENS";
[ "$CONTA"  != "$NA" ] && options="$options -A $CONTA";

runcmd $FQCLEANER $infiles $options -s $steps > $OUTDIR/$BASENAME.stepI.log ;

if [ $? -ne 0 ]; then echo "error in fqCleanER" >&2 ; exit 1 ; fi

F11=$NA; F12=$NA; F1S=$NA;   F21=$NA; F22=$NA; F2S=$NA;    F31=$NA; F32=$NA; F3S=$NA;
if [ $NLIB -eq 1 ]
then
  F11=$TMP_DIR/stepI.1.fastq; F12=$TMP_DIR/stepI.2.fastq; F1S=$TMP_DIR/stepI.S.fastq;
  [ ! -s $F1S ] && head -4 $F12 > $F1S ;
else
  $LIB1 && { F11=$TMP_DIR/stepI.1.1.fastq; F12=$TMP_DIR/stepI.1.2.fastq; F1S=$TMP_DIR/stepI.1.S.fastq; [ ! -s $F1S ] && head -4 $F12 > $F1S ; }
  $LIB2 && { F21=$TMP_DIR/stepI.2.1.fastq; F22=$TMP_DIR/stepI.2.2.fastq; F2S=$TMP_DIR/stepI.2.S.fastq; [ ! -s $F2S ] && head -4 $F22 > $F2S ; }
  $LIB3 && { F31=$TMP_DIR/stepI.3.1.fastq; F32=$TMP_DIR/stepI.3.2.fastq; F3S=$TMP_DIR/stepI.3.S.fastq; [ ! -s $F3S ] && head -4 $F32 > $F3S ; }
fi

runcmd $FQSTATS $TMP_DIR/stepI.*.fastq | $BAWK '{print $2"\t"$4"\t"int(0.5 + $4/$2)}' > $TMP_DIR/stepI.arl ;

echo "[ok]" ;

### steps 2: N|MN ############################################################
MESSAGE="Merging and/or Normalizing reads"
echo -n "$(chrono) $MESSAGE ... " ;

infiles="";
$LIB1 && infiles="$infiles -1 $F11 -2 $F12 -7 $F1S";
$LIB2 && infiles="$infiles -3 $F21 -4 $F22 -8 $F2S";
$LIB3 && infiles="$infiles -5 $F31 -6 $F32 -9 $F3S";
# step 2n: N
steps="N";  options="-o $TMP_DIR -b stepN -q $Q -p $P -c $MINCOV -C $MAXCOV -t 1 -w $TMP_DIR";  [ $L -gt 0 ] && options="$options -l $L";
runcmd $FQCLEANER $infiles $options -s $steps > $OUTDIR/$BASENAME.stepN.log & 
# step 2m: MN
steps="MN"; options="-o $TMP_DIR -b stepM -q $Q -p $P -c $MINCOV -C $MAXCOV -t 1 -w $TMP_DIR";  [ $L -gt 0 ] && options="$options -l $L";
runcmd $FQCLEANER $infiles $options -s $steps > $OUTDIR/$BASENAME.stepM.log & 

wait 2>/dev/null ;

FN11=$NA; FN12=$NA; FN1S=$NA;             FN21=$NA; FN22=$NA; FN2S=$NA;             FN31=$NA; FN32=$NA; FN3S=$NA;
FM11=$NA; FM12=$NA; FM1S=$NA; FM1M=$NA;   FM21=$NA; FM22=$NA; FM2S=$NA; FM2M=$NA;   FM31=$NA; FM32=$NA; FM3S=$NA; FM3M=$NA; 
if $LIB1
then
  FN11=$TMP_DIR/stepN.1.1.fastq; FN12=$TMP_DIR/stepN.1.2.fastq; FN1S=$TMP_DIR/stepN.4.fastq;
  [ ! -s $FN1S ] && head -4 $FN12 > $FN1S ; 
  FM11=$TMP_DIR/stepM.1.1.fastq; FM12=$TMP_DIR/stepM.1.2.fastq; FM1S=$TMP_DIR/stepM.4.fastq; FM1M=$TMP_DIR/stepM.1.M.fastq;
  [ ! -s $FM1S ] && head -4 $FM12 > $FM1S ;
  [ ! -s $FM1M ] && head -4 $FM11 > $FM1M ; 
fi
if $LIB2
then
  FN21=$TMP_DIR/stepN.2.1.fastq; FN22=$TMP_DIR/stepN.2.2.fastq; FN2S=$TMP_DIR/stepN.5.fastq;
  [ ! -s $FN2S ] && head -4 $FN22 > $FN2S ; 
  FM21=$TMP_DIR/stepM.2.1.fastq; FM22=$TMP_DIR/stepM.2.2.fastq; FM2S=$TMP_DIR/stepM.5.fastq; FM2M=$TMP_DIR/stepM.2.M.fastq;
  [ ! -s $FM2S ] && head -4 $FM22 > $FM2S ;
  [ ! -s $FM2M ] && head -4 $FM21 > $FM2M ; 
fi
if $LIB3
then
  FN31=$TMP_DIR/stepN.3.1.fastq; FN32=$TMP_DIR/stepN.3.2.fastq; FN3S=$TMP_DIR/stepN.6.fastq;
  [ ! -s $FN3S ] && head -4 $FN32 > $FN3S ; 
  FM31=$TMP_DIR/stepM.3.1.fastq; FM32=$TMP_DIR/stepM.3.2.fastq; FM3S=$TMP_DIR/stepM.6.fastq; FM3M=$TMP_DIR/stepM.3.M.fastq;
  [ ! -s $FM3S ] && head -4 $FM32 > $FM3S ;
  [ ! -s $FM3M ] && head -4 $FM31 > $FM3M ; 
fi
echo "[ok]" ;

##############################################################################
## approximating genome length                                              ##
##############################################################################
MESSAGE="Approximating genome size"
echo -n "$(chrono) $MESSAGE ... " ;

runcmd $NTCARD -t $(( $NTHREADS / 2 )) -o $TMP_DIR/stepN.fi $(ls $TMP_DIR/stepN.*.fastq | grep -v -F ".2.fastq") 2> $TMP_DIR/stepN.F1F0 & 
runcmd $NTCARD -t $(( $NTHREADS / 2 )) -o $TMP_DIR/stepM.fi      $TMP_DIR/stepM.*.fastq                          2> $TMP_DIR/stepM.F1F0 &
runcmd $FQSTATS $TMP_DIR/stepN.*.fastq | $BAWK '{print $2"\t"$4"\t"int(0.5 + $4/$2)}' > $TMP_DIR/stepN.arl &
runcmd $FQSTATS $TMP_DIR/stepM.*.fastq | $BAWK '{print $2"\t"$4"\t"int(0.5 + $4/$2)}' > $TMP_DIR/stepM.arl &

wait 2>/dev/null ; 

NF0=$($TAWK '($2=="F0"){print$3;exit}' $TMP_DIR/stepN.F1F0);
gsizeN=$($TAWK -v s=$NF0 '(NR==1){next}
                          ($2==1){s-=$3}
                          ($2==2){s-=$3}
                          END    {print s}' $TMP_DIR/stepN.fi);
MF0=$($TAWK '($2=="F0"){print$3;exit}' $TMP_DIR/stepM.F1F0);
gsizeM=$($TAWK -v s=$MF0 '(NR==1){next}
                          ($2==1){s-=$3}
                          ($2==2){s-=$3}
                          END    {print s}' $TMP_DIR/stepM.fi);
GSIZE=$(( ( $gsizeN + $gsizeM ) / 2 ));
arlN=$($TAWK '{print$3}' $TMP_DIR/stepN.arl); 
arlM=$($TAWK '{print$3}' $TMP_DIR/stepM.arl); 
echo "[ok]" ;
echo "> $GSIZE bps (N=$gsizeN M=$gsizeM)" ;

##############################################################################
## assembling genomes                                                       ##
##############################################################################
MESSAGE="Assembling genome with/without PE read merging (dnaM/dnaN)"
echo -n "$(chrono) $MESSAGE ... " ;

infiles="";
$LIB1 && infiles="$infiles --pe-1 1 $FN11 --pe-2 1 $FN12 --pe-s 1 $FN1S";
$LIB2 && infiles="$infiles --pe-1 2 $FN21 --pe-2 2 $FN22 --pe-s 2 $FN2S";
$LIB3 && infiles="$infiles --pe-1 3 $FN31 --pe-2 3 $FN32 --pe-s 3 $FN3S";
ks="21,33";
cov=$($TAWK -v lgt=$GSIZE '{print int(0.5 + $2/lgt)}' $TMP_DIR/stepN.arl);
if [ $cov -gt 50 ]
then
  [ $arlN -ge 74 ] && ks="$ks,55"; [ $arlN -ge 96 ] && ks="$ks,77";  [ $arlN -ge 118 ] && ks="$ks,99";
  if [ $arlN -ge 146 ]; then k=$(( $arlN - 30 )); [ $(( $k % 2 )) -eq 0 ] && k=$(( $k + 1 )); [ $k -ge 121 ] && k=121; ks="$ks,$k"; fi
  runcmd $SPADES $infiles -o $TMP_DIR/dnaN -t $(( $NTHREADS / 2 )) -k $ks --isolate --only-assembler &> /dev/null &
  infoN="> [dnaN]  covr=$cov   arl=$arlN   k=$ks";
else
  [ $arlN -ge 82 ] && ks="$ks,55"; [ $arlN -ge 115 ] && ks="$ks,77"; [ $arlN -ge 151 ] && ks="$ks,99"; [ $arlN -ge 251 ] && ks="$ks,121";
  runcmd $SPADES $infiles -o $TMP_DIR/dnaN -t $(( $NTHREADS / 2 )) -k $ks --careful --only-assembler &> /dev/null &
  infoN="> [dnaN]  covr=$cov   arl=$arlN   k=$ks";   
fi

infiles="";
$LIB1 && infiles="$infiles --pe-1 1 $FM11 --pe-2 1 $FM12 --pe-s 1 $FM1S --pe-m 1 $FM1M";
$LIB2 && infiles="$infiles --pe-1 2 $FM21 --pe-2 2 $FM22 --pe-s 2 $FM2S --pe-m 2 $FM2M";
$LIB3 && infiles="$infiles --pe-1 3 $FM31 --pe-2 3 $FM32 --pe-s 3 $FM3S --pe-m 3 $FM3M";
ks="21,33";
cov=$($TAWK -v lgt=$GSIZE '{print int(0.5 + $2/lgt)}' $TMP_DIR/stepM.arl);
if [ $cov -gt 50 ]
then
  [ $arlM -ge 74 ] && ks="$ks,55"; [ $arlM -ge 96 ] && ks="$ks,77";  [ $arlM -ge 118 ] && ks="$ks,99"; 
  if [ $arlM -ge 146 ]; then k=$(( $arlM - 30 )); [ $(( $k % 2 )) -eq 0 ] && k=$(( $k + 1 )); [ $k -ge 121 ] && k=121; ks="$ks,$k"; fi
  runcmd $SPADES $infiles -o $TMP_DIR/dnaM -t $(( $NTHREADS / 2 )) -k $ks --isolate --only-assembler &> /dev/null &
  infoM="> [dnaM]  covr=$cov   arl=$arlM   k=$ks" ;   
else
  [ $arlM -ge 82 ] && ks="$ks,55"; [ $arlM -ge 115 ] && ks="$ks,77"; [ $arlM -ge 151 ] && ks="$ks,99"; [ $arlM -ge 251 ] && ks="$ks,121";
  runcmd $SPADES $infiles -o $TMP_DIR/dnaM -t $(( $NTHREADS / 2 )) -k $ks --careful --only-assembler &> /dev/null &
  infoM="> [dnaM]  covr=$cov   arl=$arlM   k=$ks" ;   
fi

wait 2>/dev/null ;

echo "[ok]" ;

# rewriting FASTA headers: NODE_ID_length_LGT_covk_CK
# discarding scaffolds of length < $arl
arl=$($TAWK '{print$3}' $TMP_DIR/stepI.arl); 
echo "$infoN" ;
if [ -s $TMP_DIR/dnaN/scaffolds.fasta ]
then $TAWK -v l=$arl '/^>/{if(length(s)>=l)print h"\n"s;
                           s=""; h=$0;
                           next;
                          } 
                          {s=s$0} 
                      END {if(length(s)>=l)print h"\n"s}' $TMP_DIR/dnaN/scaffolds.fasta | sed 's/_cov_/_covk_/g' > $TMP_DIR/dnaN.fasta ; 
else echo "[WARNING] dnaN leads to empty scaffolds.fasta"  >&2 ;
fi
echo "$infoM" ;
if [ -s $TMP_DIR/dnaM/scaffolds.fasta ]
then $TAWK -v l=$arl '/^>/{if(length(s)>=l)print h"\n"s;
                           s=""; h=$0;
                           next;
                          } 
                          {s=s$0} 
                      END {if(length(s)>=l)print h"\n"s}' $TMP_DIR/dnaM/scaffolds.fasta | sed 's/_cov_/_covk_/g' > $TMP_DIR/dnaM.fasta ; 
else echo "[WARNING] dnaM leads to empty scaffolds.fasta"  >&2 ;
fi

##############################################################################
## exit when no scaffold sequence was written                               ##
##############################################################################
if [ ! -e $TMP_DIR/dnaN.fasta ] && [ ! -e $TMP_DIR/dnaM.fasta ]
then 
  cp $TMP_DIR/dnaN/spades.log $OUTDIR/$BASENAME.dnaN.spades.log ; 
  cp $TMP_DIR/dnaM/spades.log $OUTDIR/$BASENAME.dnaM.spades.log ; 

  $RMTMP && rm -rf $TMP_DIR/ ;

  echo "> dnaN log: $OUTDIR/$BASENAME.dnaN.spades.log" ;
  echo "> dnaM log: $OUTDIR/$BASENAME.dnaM.spades.log" ;
  echo "$(chrono) exit " ;

  exit ;
fi

##############################################################################
## selecting the most accurate assembly                                     ##
##############################################################################
MESSAGE="Comparing genome assemblies"
echo -n "$(chrono) $MESSAGE ... " ;
nseqN=0; nresN=0; augN=0; ng50N=0;
if [ -e $TMP_DIR/dnaN.fasta ]
then 
  runcmd $FASTA2AGP -i $TMP_DIR/dnaN.fasta -o $TMP_DIR/ctgN.fasta -a /dev/null -c $MINCTGLGT ;
  statN="$($CONTIG_INFO -t -g $GSIZE $TMP_DIR/ctgN.fasta | sed -n 2p)";
  rm -f $TMP_DIR/ctgN.fasta ;
  nseqN=$($TAWK '{print $2}' <<<"$statN"); nresN=$($TAWK '{print ($3-$8)}' <<<"$statN");
  augN=$($TAWK '{print $22}' <<<"$statN"); ng50N=$($TAWK '{print $23}' <<<"$statN"); 
fi
nseqM=0; nresM=0; augM=0; ng50M=0;
if [ -e $TMP_DIR/dnaM.fasta ]
then 
  runcmd $FASTA2AGP -i $TMP_DIR/dnaM.fasta -o $TMP_DIR/ctgM.fasta -a /dev/null -c $MINCTGLGT ;
  statM="$($CONTIG_INFO -t -g $GSIZE $TMP_DIR/ctgM.fasta | sed -n 2p)";
  rm -f $TMP_DIR/ctgM.fasta ;
  nseqM=$($TAWK '{print $2}' <<<"$statM"); nresM=$($TAWK '{print ($3-$8)}' <<<"$statM");
  augM=$($TAWK '{print $22}' <<<"$statM"); ng50M=$($TAWK '{print $23}' <<<"$statM"); 
fi
echo "[ok]" ;
echo "> [dnaN]  Nseq=$nseqN   Nres=$nresN   NG50=$ng50N   auGN=$augN" ;
echo "> [dnaM]  Nseq=$nseqM   Nres=$nresM   NG50=$ng50M   auGN=$augM" ;

SCFDO=$TMP_DIR/dna.scfo.fasta;
BESTDNA="";
if [ $augN -gt $augM ]
then
  BESTDNA="N";
  echo "> selecting dnaN" ;
  sed 's/_length_.*_covk_/_covk_/g' $TMP_DIR/dnaN.fasta > $SCFDO ;
  mv $TMP_DIR/dnaN/ $TMP_DIR/dna/ ;
  rm -rf $TMP_DIR/stepM.* $TMP_DIR/dnaN.fasta $TMP_DIR/dnaM.fasta $TMP_DIR/dnaM/ ;
else
  BESTDNA="M";
  echo "> selecting dnaM" ;
  sed 's/_length_.*_covk_/_covk_/g' $TMP_DIR/dnaM.fasta > $SCFDO ;
  mv $TMP_DIR/dnaM/ $TMP_DIR/dna/ ;
  rm -rf $TMP_DIR/stepN.* $TMP_DIR/dnaN.fasta $TMP_DIR/dnaM.fasta $TMP_DIR/dnaN/ ;
fi

# displaying stats
runcmd $FASTA2AGP -i $SCFDO -o $TMP_DIR/ctg.fasta -a /dev/null -c $MINCTGLGT ;
runcmd $CONTIG_INFO -t $TMP_DIR/ctg.fasta | sed -n 2p | $TAWK '{print "> [dna]   Nseq="$2"   Nres="($3-$8)"    N50="$23"    auN="$22}' ;
rm -f $TMP_DIR/ctg.fasta ;

##############################################################################
## mapping                                                                  ##
##############################################################################
MESSAGE="Aligning PE reads against scaffolds"
echo -n "$(chrono) $MESSAGE " ;

# indexing the scaffolds
mapref=${SCFDO%.*}.ref;
runcmd $BWA_INDEX -p $mapref $SCFDO &>/dev/null ;

# is250: %PE reads with insert size > 250
ISS=$TMP_DIR/iss.txt;
{ [ $NLIB -eq 1 ] && echo "#avg 95%is is250 mode" || echo "#lib avg 95%is is250 mode" ; } > $ISS ;
ISD=$TMP_DIR/isd.txt;

echo -n "." ;

infiles="";
if $LIB1
then
  runcmd $BWA_MEM -t $NTHREADS $mapref $F11 $F12 2>/dev/null > $TMP_DIR/lib1.sam ;
  $TAWK '($9>0){($9>max)&&max=$9; is[$9]++}
         END   {while(++i<=max)if(is[i]>0)print i" "is[i]}' $TMP_DIR/lib1.sam > $ISD ;
  is_mode=$(sort -k 2nr $ISD | head -1 | awk '{print $1}');
  istat="$($BAWK '   {n=$2; while(--n>=0)i[++l]=$1}
                  END{l/=40; n=m025=int(l); m975=int(39*l);
                      while(++n<=m975){avg+=i[n]; (i[n]>250)&&is250++} 
                      print int(avg/(m975-m025))" ["i[m025]","i[m975]"] "(100*is250/m975)}' $ISD)";
  { [ $NLIB -eq 1 ] && echo "$istat $is_mode" || echo "lib1 $istat $is_mode" ; } >> $ISS ;
  [[ "ABPSV" =~ $STRATEGY ]] && runcmd $SAMTOOLS_SORT --threads $NTHREADS -T $TMP_DIR -o $TMP_DIR/lib1.bam $TMP_DIR/lib1.sam &>/dev/null ;
  infiles="$infiles $TMP_DIR/lib1.bam" ;
  rm -f $TMP_DIR/lib1.sam ;
fi
echo -n "." ;
if $LIB2
then
  runcmd $BWA_MEM -t $NTHREADS $mapref $F21 $F22 2>/dev/null > $TMP_DIR/lib2.sam ;
  $TAWK '($9>0){($9>max)&&max=$9; is[$9]++}
         END   {while(++i<=max)if(is[i]>0)print i" "is[i]}' $TMP_DIR/lib2.sam > $ISD ;
  is_mode=$(sort -k 2nr $ISD | head -1 | awk '{print $1}');
  istat="$($BAWK '   {n=$2; while(--n>=0)i[++l]=$1}
                  END{l/=40; n=m025=int(l); m975=int(39*l);
                      while(++n<=m975){avg+=i[n]; (i[n]>250)&&is250++} 
                      print int(avg/(m975-m025))" ["i[m025]","i[m975]"] "(100*is250/m975)}' $ISD)";
  { [ $NLIB -eq 1 ] && echo "$istat $is_mode" || echo "lib2 $istat $is_mode" ; } >> $ISS ; 
  [[ "ABPSV" =~ $STRATEGY ]] && runcmd $SAMTOOLS_SORT --threads $NTHREADS -T $TMP_DIR -o $TMP_DIR/lib2.bam $TMP_DIR/lib2.sam &>/dev/null ;
  infiles="$infiles $TMP_DIR/lib2.bam" ;
  rm -f $TMP_DIR/lib2.sam ;
fi
echo -n "." ;
if $LIB3
then
  runcmd $BWA_MEM -t $NTHREADS $mapref $F31 $F32 2>/dev/null > $TMP_DIR/lib3.sam ;
  $TAWK '($9>0){($9>max)&&max=$9; is[$9]++}
         END   {while(++i<=max)if(is[i]>0)print i" "is[i]}' $TMP_DIR/lib3.sam > $ISD ;
  is_mode=$(sort -k 2nr $ISD | head -1 | awk '{print $1}');
  istat="$($BAWK '   {n=$2; while(--n>=0)i[++l]=$1}
                  END{l/=40; n=m025=int(l); m975=int(39*l);
                      while(++n<=m975){avg+=i[n]; (i[n]>250)&&is250++} 
                      print int(avg/(m975-m025))" ["i[m025]","i[m975]"] "(100*is250/m975)}' $ISD)";
  { [ $NLIB -eq 1 ] && echo "$istat $is_mode" || echo "lib3 $istat $is_mode" ; } >> $ISS ; 
  [[ "ABPSV" =~ $STRATEGY ]] && runcmd $SAMTOOLS_SORT --threads $NTHREADS -T $TMP_DIR -o $TMP_DIR/lib3.bam $TMP_DIR/lib3.sam &>/dev/null ;
  infiles="$infiles $TMP_DIR/lib3.bam" ;
  rm -f $TMP_DIR/lib3.sam ;
fi

rm -f $mapref.* ;

echo -n "." ;

# preparing sorted bam files for polishing
if [[ "ABPSV" =~ $STRATEGY ]]
then
  if [ $NLIB -eq 1 ]
  then mv $infiles $TMP_DIR/aln.bam ;
  else runcmd $SAMTOOLS_MERGE --threads $NTHREADS -o $TMP_DIR/aln.bam $infiles &>/dev/null ; rm -f $infiles ;
  fi
fi

echo " [ok]" ;

##############################################################################
## polishing                                                                ##
##############################################################################
SCFD=$TMP_DIR/dna.scf.fasta;
MSCFD=$TMP_DIR/dna.mod.fasta;
if [[ "ABPSV" =~ $STRATEGY ]]
then
  MESSAGE="Polishing scaffolds";
  echo "$(chrono) $MESSAGE ... " ;

  #####################
  ### polishing 1 #####
  #####################
  # modifying the scaffolds to maximize their 'fit' against the PE reads
  STCopt="--mode bayesian --min-MQ 2 --P-het 1.0e-100 --het-scale 1.0";
  runcmd $SAMTOOLS_CONSENSUS $STCopt --threads $NTHREADS $TMP_DIR/aln.bam 2>/dev/null | $BAWK '!/^>/  {s=s$0;next} 
                                                                                               (s!=""){print s; s=""} 
                                                                                                      {print} 
                                                                                               END    {print s}' > $SCFD ;
  # stats
  sed 's/^[nN]*//;s/[nN]*$//' $SCFD | paste - - | $TAWK '(length($2)>0){print$1;print$2}' > $MSCFD ; ## discarding suffix/prefix Ns, as well as zero-length contigs (if any)
  runcmd $FASTA2AGP -i $MSCFD -o $TMP_DIR/ctg.fasta -a /dev/null -c $MINCTGLGT ;
  stats="$(runcmd $CONTIG_INFO -t -g $GSIZE $TMP_DIR/ctg.fasta | sed -n 2p)";
  rm -f $TMP_DIR/ctg.fasta ;
  $TAWK '{print "> [dna]   Nseq="$2"   Nres="($3-$8)"   NG50="$23"   auGN="$22}' <<<"$stats" ;

  #####################
  ### polishing 2 #####
  #####################
  # indexing the scaffolds
  mapref=${MSCFD%.*}.ref;
  runcmd $BWA_INDEX -p $mapref $MSCFD &>/dev/null ;
  # mapping
  thr_mem=$(( 66 * $NTHREADS / 100 )); [ $thr_mem -le 0 ] && thr_mem=1;
  thr_srt=$(( $NTHREADS - $thr_mem )); [ $thr_srt -le 0 ] && thr_srt=1;
  infiles="";
  if $LIB1
  then               runcmd $BWA_MEM -t $thr_mem $mapref $F11 $F12 2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lib1.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lib1.bam" ;
    [ -s $F1S ] && { runcmd $BWA_MEM -t $thr_mem $mapref $F1S      2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lis1.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lis1.bam" ; }
  fi
  if $LIB2
  then               runcmd $BWA_MEM -t $thr_mem $mapref $F21 $F22 2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lib2.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lib2.bam" ;
    [ -s $F2S ] && { runcmd $BWA_MEM -t $thr_mem $mapref $F2S      2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lis2.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lis2.bam" ; }
  fi
  if $LIB3
  then               runcmd $BWA_MEM -t $thr_mem $mapref $F31 $F32 2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lib3.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lib3.bam" ;
    [ -s $F3S ] && { runcmd $BWA_MEM -t $thr_mem $mapref $F3S      2>/dev/null | $SAMTOOLS_SORT --threads $thr_srt -T $TMP_DIR -o $TMP_DIR/lis3.bam &>/dev/null ; infiles="$infiles $TMP_DIR/lis3.bam" ; }
  fi
  rm -f $mapref.* ;
  # merging
  rm -f $TMP_DIR/aln.bam ;
  if [ $(wc -w <<<"$infiles") -eq 1 ]
  then mv $infiles $TMP_DIR/aln.bam ;
  else runcmd $SAMTOOLS_MERGE --threads $NTHREADS -o $TMP_DIR/aln.bam $infiles &>/dev/null ; rm -f $infiles ;
  fi
  # modifying the scaffolds using a simple consensus approach
  STCopt="--mode simple --min-MQ 20 --use-qual --min-depth 1 --call-fract 0.75 --het-fract 0.2";
  runcmd $SAMTOOLS_CONSENSUS $STCopt --threads $NTHREADS $TMP_DIR/aln.bam 2>/dev/null | $BAWK '!/^>/  {s=s$0;next} 
                                                                                               (s!=""){print s; s=""} 
                                                                                                      {print} 
                                                                                               END    {print s}' > $SCFD ;
  rm -f $TMP_DIR/aln.bam ;
  # stats
  sed 's/^[nN]*//;s/[nN]*$//' $SCFD | paste - - | $TAWK '(length($2)>0){print$1;print$2}' > $MSCFD ; ## discarding suffix/prefix Ns, as well as zero-length contigs (if any)
  runcmd $FASTA2AGP -i $MSCFD -o $TMP_DIR/ctg.fasta -a /dev/null -c $MINCTGLGT ;
  runcmd $CONTIG_INFO -t -g $GSIZE $TMP_DIR/ctg.fasta | sed -n 2p | $TAWK '{print "> [dna]   Nseq="$2"   Nres="($3-$8)"   NG50="$23"   auGN="$22}' ;
  rm -f $TMP_DIR/ctg.fasta ;

  mv $MSCFD $SCFD ;

  # displaying stats
  runcmd $FASTA2AGP -i $SCFD -o $TMP_DIR/ctg.fasta -a /dev/null -c $MINCTGLGT ;
  runcmd $CONTIG_INFO -t $TMP_DIR/ctg.fasta | sed -n 2p | $TAWK '{print "> [dna]   Nseq="$2"   Nres="($3-$8)"    N50="$23"    auN="$22}' ;
  rm -f $TMP_DIR/ctg.fasta ;
fi

##############################################################################
## post-processing                                                          ##
##############################################################################
MESSAGE="Processing scaffolds";
echo -n "$(chrono) $MESSAGE " ;

# indexing the scaffolds
mapref=${SCFD%.*}.ref;
runcmd $BWA_INDEX -p $mapref $SCFD &>/dev/null ;

PESAM=$TMP_DIR/pe.sam; touch $PESAM ;
SESAM=$TMP_DIR/se.sam; touch $SESAM ;
if [ "$BESTDNA" == "N" ]
then
  $LIB1 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN11 $FN12 2>/dev/null >> $PESAM ; }
  $LIB1 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN1S       2>/dev/null >> $SESAM ; }
  $LIB2 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN21 $FN22 2>/dev/null >> $PESAM ; }
  $LIB2 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN2S       2>/dev/null >> $SESAM ; }
  $LIB3 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN31 $FN32 2>/dev/null >> $PESAM ; }
  $LIB3 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FN3S       2>/dev/null >> $SESAM ; }
else
  $LIB1 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM11 $FM12 2>/dev/null >> $PESAM ; }
  $LIB1 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM1S       2>/dev/null >> $SESAM ; }
  $LIB1 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM1M       2>/dev/null >> $SESAM ; }
  $LIB2 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM21 $FM22 2>/dev/null >> $PESAM ; }
  $LIB2 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM2S       2>/dev/null >> $SESAM ; }
  $LIB2 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM2M       2>/dev/null >> $SESAM ; }
  $LIB3 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM31 $FM22 2>/dev/null >> $PESAM ; }
  $LIB3 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM3S       2>/dev/null >> $SESAM ; }
  $LIB3 && { echo -n "." ; runcmd $BWA_MEM -t $NTHREADS $mapref $FM3M       2>/dev/null >> $SESAM ; }
fi   
rm -f $mapref.* ;

runcmd grep -v "^@" $PESAM $SESAM | $SAM2MAP -i - -r $SCFD -q $Q -o $TMP_DIR/s2m ;

# average coverage depth estimated from observed coverage distribution
wcov=$($BAWK '/^cov/   {r=1;next}
              /^>/     {printf("%.4f", x/y);exit}
              (r&&$1>0){c=$1;x+=c*$NF;y+=$NF}' $TMP_DIR/s2m.cov.txt);
# mode of the observed coverage distribution
mode=$($BAWK 'BEGIN       {mod=max=0}
              /^cov/      {r=1;next}
              /^>/        {print mod;exit}
              (r&&$NF>max){max=$NF;mod=$1}' $TMP_DIR/s2m.cov.txt);
# unimodality coefficient of the observed coverage distribution
#  also called momo (= mono-modality) coefficient
#
#                         o                      o* observed distribution
#                        o|o                     o- the mono-modal distribution that 'includes' 
#                       o | o                        the observed distribution with minimum AUC
#         o------------o  |  o   
#        o **         *   |   ooo                  unimodality index = AUC(o*) / AUC(o-)
#       o    ***    **    |      ooo---oo            > 0.99 => unimodal
#      o        ****      |         * *  oo          < 0.99 => multimodal
#    oo                   |          *     oooooo 
#    |====================|=====================|
#   xmin                 mode                  xmax 
#
momo=$($BAWK -v m=$mode '/^cov/   {r=1;next}
                         /^>/     {x=1;    h=d[x]; area_up=d[x];  area_dn=h;
                                   while(++x<=m){h=(d[x]<h)?h:d[x]; area_up+=d[x]; area_dn+=h}
                                   x=xmax; h=d[x]; area_up+=d[x]; area_dn+=h;
                                   while(--x>m) {h=(d[x]<h)?h:d[x]; area_up+=d[x]; area_dn+=h}
                                   printf("%.6f", area_up/area_dn); exit;
                                  }
                         (r&&$1>0){d[$1]=$NF;xmax=$1}' $TMP_DIR/s2m.cov.txt);
# accuracy index = momo * min [ wcov/MAXCOV , mode/wcov ]
accuracy=$($BAWK 'function min(x,y){return ((x<y)?x:y)}
                                   {printf("%.6f", $1 *  min($3/$4, $2/$3))}' <<<"$momo $mode $wcov $MAXCOV");
# low coverage depth cutoff
cutoff=$(grep " min-" $TMP_DIR/s2m.cov.txt  | $BAWK '{print$1}');

# rewriting s2m.cov.txt
sed -n 1p $TMP_DIR/s2m.cov.txt                                        > $TMP_DIR/s2m.cov.tmp ;
echo -e "   avg=$wcov  mode=$mode  momo=$momo  accuracy=$accuracy\n" >> $TMP_DIR/s2m.cov.tmp ;
sed -n 2,3p $TMP_DIR/s2m.cov.txt                                     >> $TMP_DIR/s2m.cov.tmp ;
echo -e "   min.cov.cutoff=$cutoff (p-value<=0.000001)\n"            >> $TMP_DIR/s2m.cov.tmp ;
sed 1,3d $TMP_DIR/s2m.cov.txt                                        >> $TMP_DIR/s2m.cov.tmp ;
mv $TMP_DIR/s2m.cov.tmp  $TMP_DIR/s2m.cov.txt ;

echo ". [ok]" ;

echo "> expect. cov.        $MAXCOV" ;
echo "> avg. cov.           $wcov" ;
echo "> cov. mode           $mode" ;
echo "> momo index          $momo" ;
echo "> accuracy            $accuracy" ;
echo "> min. cov. cutoff    $cutoff" ;

# rewriting FASTA header: NODE_ID_covk_CK_covr_CR_cutoff_CO
# rewriting scaffolds:    lower case for bases that are insufficiently covered
$TAWK -v m=$cutoff '/>/     {if(d!=0)print fh"_covr_"sprintf("%.3f",d/n)"_cutoff_"m"\n"s;
                             fh=$1; d=n=0; s="";
                             next;
                            }
                    (NF==11){d+=$4+$5+$6+$7; ++n;
                             s=s""(($10=="U")?tolower($2):$2);
                            }
                    END     {print fh"_covr_"sprintf("%.3f",d/n)"_cutoff_"m"\n"s}' $TMP_DIR/s2m.map > $SCFD ;


# discarding scaffolds of length < MINCTGLGT and with covr < cutoff
SCFDFLT=$TMP_DIR/dna.scflt.fasta;
paste - - < $SCFD | tr '_' '\t' | $TAWK -v min=$MINCTGLGT '(length($9)>=min&&$6>=$8){print$1"_"$2"_"$3"_"$4"_"$5"_"$6"_"$7"_"$8"\n"$9}' > $SCFDFLT ;

# getting troublesome positions
# => assembled base N whose (i) coverage depth >cutoff and (ii) proportion of the majority sequenced base <f (f=80%)
TRBPOS=$TMP_DIR/s2m.pos;
grep "^>" $SCFDFLT > $TMP_DIR/tmp.txt ;
$TAWK -v f=80 -v m=$cutoff 'BEGIN        {print"#seq\tlgt\tpos\tbase\t%A\t%C\t%G\t%T\t%gap"; 
                                          ok=false;
                                         }
                            (NR==FNR)    {h1=$0; sub("_covr_.*","", h1);
                                          id=$0; sub("_covk_.*","",id); sub(">NODE_","",id);
                                          fh[h1]="NODE_"sprintf("%04d",id);
                                          next;
                                         }
                            /^>/         {h=$1; l=$2;
                                          ok=($1 in fh);
                                          next;
                                         }
                            (ok&&$2=="N"){s=$4+$5+$6+$7+$8;
                                          if(s<m){next}
                                          max=0;
                                          fa=100*$4/s; if(fa>max)max=fa;
                                          fc=100*$5/s; if(fc>max)max=fc;
                                          fg=100*$6/s; if(fg>max)max=fg;
                                          ft=100*$7/s; if(ft>max)max=ft;
                                          fx=100*$8/s; if(fx>max)max=fx;
                                          if(max>=f){next}
                                          printf(fh[h]"\t"l"\t"$1"\tN\t");
                                          printf(((fa==0)?"0\t":"%.1f\t"), fa);
                                          printf(((fc==0)?"0\t":"%.1f\t"), fc);
                                          printf(((fg==0)?"0\t":"%.1f\t"), fg);
                                          printf(((ft==0)?"0\t":"%.1f\t"), ft);
                                          printf(((fx==0)?"0\n":"%.1f\n"), fx);}' $TMP_DIR/tmp.txt  $TMP_DIR/s2m.map  >  $TRBPOS ;

echo "> ambiguous pos.      $(sed 1d $TRBPOS | wc -l)" ;

# rewriting scaffold headers:                     [--- NODE ------------]  [--- lgt -----]   [--- covk -------------]   [--- covr -------------]   [cutoff]
paste - - < $SCFDFLT | tr '_' '\t' | $TAWK '{print$1"_"sprintf("%04d",$2)"_lgt_"length($9)"_"$3"_"sprintf("%.2f", $4)"_"$5"_"sprintf("%.2f", $6)"_"$7"_"$8"\n"$9}' > $TMP_DIR/tmp.fasta ;
mv $TMP_DIR/tmp.fasta $SCFDFLT ;

# displaying stats
runcmd $FASTA2AGP -i $SCFDFLT -o $TMP_DIR/ctg.fasta -a /dev/null -c $MINCTGLGT ;
runcmd $CONTIG_INFO -t $TMP_DIR/ctg.fasta | sed -n 2p | $TAWK '{print "> [dna]   Nseq="$2"   Nres="($3-$8)"    N50="$23"    auN="$22}' ;
rm -f $TMP_DIR/ctg.fasta ;


##############################################################################
## annotating                                                               ##
##############################################################################
if [[ "ABV" =~ $STRATEGY ]]
then
  MESSAGE="Annotating scaffold sequences"
  echo -n "$(chrono) $MESSAGE ." ;

  GENUS="$(cut -d" "   -f1  <<<"$TAXON" | sed 's/^_*//' | cut -d"_" -f1)";
  SPECIES="$(cut -d" " -f2  <<<"$TAXON" | sed 's/^_*//' | cut -d"_" -f1)";
  STRAIN="$(cut -d" "  -f3- <<<"$TAXON")";
  lt1=$(tr -d '.'    <<<"$GENUS");   lt1=${lt1:0:3};
  lt2=$(tr -d '.'    <<<"$SPECIES"); lt2=${lt2:0:3};
  lt3=$(tr -d '_ -.' <<<"$STRAIN");
  [ "${lt2^^}" == "SP" ] && LOCUSTAG="$lt1$lt3" || LOCUSTAG="$lt1$lt2$lt3";
  [ "${LOCUSTAG^^}" == "GENSTR" ] && LOCUSTAG="L$BASENAME" ;
  LOCUSTAG=${LOCUSTAG:0:12}
  LOCUSTAG=${LOCUSTAG^^};
  echo -n "." ;
  SCFDFLTRW=$TMP_DIR/dna.scfltrw.fasta;
  sed 's/_lgt_.*//g' $SCFDFLT > $SCFDFLTRW ;
  PROKKA_OPT="--cpus $NTHREADS --locustag $LOCUSTAG --outdir $TMP_DIR/prokka";
  case $STRATEGY in
   A) runcmd $PROKKA $PROKKA_OPT --kingdom Archaea  --genus "$GENUS" --species "$SPECIES" --strain "$STRAIN"   $SCFDFLTRW  2>/dev/null ;;
   B) runcmd $PROKKA $PROKKA_OPT --kingdom Bacteria --genus "$GENUS" --species "$SPECIES" --strain "$STRAIN"   $SCFDFLTRW  2>/dev/null ;;
   V) runcmd $PROKKA $PROKKA_OPT --kingdom Viruses  --genus "$GENUS" --species "$SPECIES" --strain "$STRAIN"   $SCFDFLTRW  2>/dev/null ;;
  esac
  echo -n "." ;
  [[ "B" =~ $STRATEGY ]] && runcmd $PLATON --threads $NTHREADS --output $TMP_DIR --prefix platon -v $SCFDFLT &> /dev/null ;

  echo " [ok]" ;

  echo "> taxon:     $GENUS $SPECIES $STRAIN" ;
  echo "> locus tag: $LOCUSTAG" ;
fi


##############################################################################
## finalizing                                                               ##
##############################################################################
echo "# output files:"

cp $SCFDO $OUTDIR/$BASENAME.all.fasta ;

echo "+ de novo assembly (all scaffolds):         $OUTDIR/$BASENAME.all.fasta" ;

cp $TMP_DIR/s2m.cov.txt $OUTDIR/$BASENAME.cov.info.txt ;
echo "+ coverage profile summary:                 $OUTDIR/$BASENAME.cov.info.txt" ;

cp $SCFDFLT $OUTDIR/$BASENAME.scf.fasta ;
echo "+ de novo assembly (selected scaffolds):    $OUTDIR/$BASENAME.scf.fasta" ;

runcmd $CONTIG_INFO -t -r $SCFDFLT | cut -f2- | sed 's/^Seq/#Seq/' | sed 's/_lgt_.*_cutoff_[0123456789]*//g' > $OUTDIR/$BASENAME.scf.info.txt ;
if [[ "B" =~ $STRATEGY ]]
then
  paste $OUTDIR/$BASENAME.scf.info.txt <( $TAWK -v cch=$PLATON_SENS -v cpl=$PLATON_SPEC '(NR==1){print"CPU";next}{print(($5<cch)?"C":($5>cpl)?"P":"U") }' $TMP_DIR/platon.tsv ) > $OUTDIR/$BASENAME.scf.info.tmp ;
  mv $OUTDIR/$BASENAME.scf.info.tmp $OUTDIR/$BASENAME.scf.info.txt ;
fi
echo "+ sequence stats (selected scaffolds):      $OUTDIR/$BASENAME.scf.info.txt" ;

cp $TRBPOS $OUTDIR/$BASENAME.scf.amb.txt ;
echo "+ ambiguous positions (selected scaffolds): $OUTDIR/$BASENAME.scf.amb.txt" ;

runcmd $FASTA2AGP -i $SCFDFLT -o $OUTDIR/$BASENAME.agp.fasta -a $OUTDIR/$BASENAME.agp -c $MINCTGLGT ;
echo "+ de novo assembly (selected contigs):      $OUTDIR/$BASENAME.agp.fasta" ;
echo "+ scaffolding info:                         $OUTDIR/$BASENAME.agp" ;

runcmd $CONTIG_INFO -t $OUTDIR/$BASENAME.all.fasta $OUTDIR/$BASENAME.scf.fasta $OUTDIR/$BASENAME.agp.fasta > $OUTDIR/$BASENAME.dna.info.txt ;
echo "+ descriptive statistics:                   $OUTDIR/$BASENAME.dna.info.txt" ;

cp $ISS $OUTDIR/$BASENAME.isd.txt ;
echo "+ insert size statistics:                   $OUTDIR/$BASENAME.isd.txt" ;

if [[ "ABV" =~ $STRATEGY ]]
then
  grep -v -F "prokka" $TMP_DIR/prokka/*.gbk > $OUTDIR/$BASENAME.scf.gbk ;
  echo "+ annotation (selected scaffolds):          $OUTDIR/$BASENAME.scf.gbk" ;
    
  cp $TMP_DIR/prokka/*.txt $OUTDIR/$BASENAME.scf.gbk.info.txt ;
  echo "+ annotation info:                          $OUTDIR/$BASENAME.scf.gbk.info.txt" ;
fi

$RMTMP && rm -rf $TMP_DIR/ ;

echo "$(chrono) exit " ;


exit ;
